Terraform EKS at scale with spot instances

![infra](./docs/AWS_infra.png)

Multi AZs EKS running in private subnet, services and api can be expose to public thru NLB and ingress

Cluster is managed by Rancher
